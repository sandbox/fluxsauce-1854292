INTRODUCTION
------------

Current Maintainer: Jon Peck <jpeck@fluxsauce.com>

Wysiwyg TinyMCE Feature is a turn-key installation and configuration of Wysiwyg
with TinyMCE and IMCE. It includes a drush make file for dependencies.

INSTALLATION
------------

drush make:

projects[wysiwyg_tinymce_feature][version] = 1.x

drush:

drush dl -y wysiwyg_tinymce_feature
drush -y en wysiwyg_tinymce_feature

DEPENDENCIES
------------

http://drupal.org/project/imce
http://drupal.org/project/imce_wysiwyg
http://drupal.org/project/wysiwyg
https://github.com/tinymce/tinymce
